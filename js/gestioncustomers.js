var clientesObtenidos;

function getClientes() {
 var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
 var request = new XMLHttpRequest();

 request.onreadystatechange = function(){
   if(this.readyState ==4 && this.status == 200){

     //console.log(request.responseText);
     clintesObtenidos = request.responseText;
     procesarclientes();
   }
 }
 request.open("GET",url,true);
 request.send();
}

function procesarclientes() {
 var JSONClientes = JSON.parse(clintesObtenidos);
 var rutabandera= "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
 var divTabla= document.getElementById("divTablaClientes");
 var tabla= document.createElement("table");
 var tbody = document.createElement("tbody");

 tabla.classList.add("table");
 tabla.classList.add("table-striped");
 //alert(JSONProductos.value[0].ProductName);
 for (var i = 0; i < JSONClientes.value.length; i++) {
 //  console.log(JSONProductos.value[i].ProductName)
 var nuevaFila = document.createElement("tr");
 var columnaNombre = document.createElement("td");
 columnaNombre.innerText = JSONClientes.value[i].ContactName;
 var columnaCity = document.createElement("td");
 columnaCity.innerText = JSONClientes.value[i].City;
 var columnaBandera = document.createElement("td");
 columnaBandera.innerText = JSONClientes.value[i].Country;
 var imgBandera = document.createElement("img");
 imgBandera.classList.add("flag");
 if(JSONClientes.value[i].Country =="UK"){
 imgBandera.src=rutabandera+"United-Kingdom.png";
}else{
 imgBandera.src=rutabandera+JSONClientes.value[i].Country+".png";
}


 nuevaFila.appendChild(columnaNombre);
 nuevaFila.appendChild(columnaCity);
 nuevaFila.appendChild(columnaBandera);
 nuevaFila.appendChild(imgBandera);

 tbody.appendChild(nuevaFila);
 }
 tabla.appendChild(tbody);
 divTabla.appendChild(tabla);
}
